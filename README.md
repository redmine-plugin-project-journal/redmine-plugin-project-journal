# Redmine plugin project journal

A Redmine plugin to journal project changes.

## Install

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-project-journal/redmine-plugin-project-journal.git plugins/project_journal
```

Restart Redmine.

## Uninstall

Remove all project journals if you want:

```bash
cd redmine
RAILS_ENV=production bin/rails project_journal:prune
```

Uninstall this plugin:

```bash
cd redmine
rm -rf plugins/project_jrounal
```

Restart Redmine.

## Usage

You can find the "History" tab in project settings page.

## Limitations

  * Project description diff isn't supported. It requires
    `JournalsController` improvement in Redmine.

  * Notification isn't supported. It requires
    `Journal#send_notification` improvement in Redmine.

## Authors

  * Sutou Kouhei `<kou@clear-code.com>`

## License

GPLv2 or later. See [LICENSE](LICENSE) for details.
